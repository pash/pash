import sys
import subprocess

oldmod = sys.modules[__name__]


class PashResult:
    def __init__(self, cmdline):
        self._cmdline = cmdline
        self._result = None

    def _doit(self):
        if self._result is None:
            self._result = subprocess.check_output(self._cmdline, shell=True)

    def __del__(self):
        # if the command hasn't run yet, then let's do it!
        self._doit()

    def __or__(self, other):
        return PashResult("%s | %s" % (self._cmdline, other._cmdline))

    def __format__(self, *args, **kwargs):
        self._doit()
        return self._result.__format__(*args, **kwargs)

    def __hash__(self):
        self._doit()
        return self._result.__hash__()

    def __reduce_ex__(self, *args, **kwargs):
        self._doit()
        return self._result.__reduce_ex__(*args, **kwargs)

    def __repr__(self):
        self._doit()
        return self._result.__repr__()

    def __sizeof__(self):
        self._doit()
        return self._result.__sizeof__()

    def __str__(self):
        self._doit()
        return self._result.__str__()

    def __add__(self, *args, **kwargs):
        self._doit()
        return self._result.__add__(*args, **kwargs)

    def __contains__(self, *args, **kwargs):
        self._doit()
        return self._result.__contains__(*args, **kwargs)

    def __eq__(self, *args, **kwargs):
        self._doit()
        return self._result.__eq__(*args, **kwargs)

    def __ge__(self, *args, **kwargs):
        self._doit()
        return self._result.__ge__(*args, **kwargs)

    def __getitem__(self, *args, **kwargs):
        self._doit()
        return self._result.__getitem__(*args, **kwargs)

    def __getnewargs__(self, *args, **kwargs):
        self._doit()
        return self._result.__getnewargs__(*args, **kwargs)

    def __getslice__(self, *args, **kwargs):
        self._doit()
        return self._result.__getslice__(*args, **kwargs)

    def __gt__(self, *args, **kwargs):
        self._doit()
        return self._result.__gt__(*args, **kwargs)

    def __le__(self, *args, **kwargs):
        self._doit()
        return self._result.__le__(*args, **kwargs)

    def __len__(self, *args, **kwargs):
        self._doit()
        return self._result.__len__(*args, **kwargs)

    def __lt__(self, *args, **kwargs):
        self._doit()
        return self._result.__lt__(*args, **kwargs)

    def __mod__(self, *args, **kwargs):
        self._doit()
        return self._result.__mod__(*args, **kwargs)

    def __mul__(self, *args, **kwargs):
        self._doit()
        return self._result.__mul__(*args, **kwargs)

    def __ne__(self, *args, **kwargs):
        self._doit()
        return self._result.__ne__(*args, **kwargs)

    def __rmod__(self, *args, **kwargs):
        self._doit()
        return self._result.__rmod__(*args, **kwargs)

    def __rmul__(self, *args, **kwargs):
        self._doit()
        return self._result.__rmul__(*args, **kwargs)

    def capitalize(self, *args, **kwargs):
        self._doit()
        return self._result.capitalize(*args, **kwargs)

    def center(self, *args, **kwargs):
        self._doit()
        return self._result.center(*args, **kwargs)

    def count(self, *args, **kwargs):
        self._doit()
        return self._result.count(*args, **kwargs)

    def decode(self, *args, **kwargs):
        self._doit()
        return self._result.decode(*args, **kwargs)

    def encode(self, *args, **kwargs):
        self._doit()
        return self._result.encode(*args, **kwargs)

    def endswith(self, *args, **kwargs):
        self._doit()
        return self._result.endswith(*args, **kwargs)

    def expandtabs(self, *args, **kwargs):
        self._doit()
        return self._result.expandtabs(*args, **kwargs)

    def find(self, *args, **kwargs):
        self._doit()
        return self._result.find(*args, **kwargs)

    def format(self, *args, **kwargs):
        self._doit()
        return self._result.format(*args, **kwargs)

    def index(self, *args, **kwargs):
        self._doit()
        return self._result.index(*args, **kwargs)

    def isalnum(self, *args, **kwargs):
        self._doit()
        return self._result.isalnum(*args, **kwargs)

    def isalpha(self, *args, **kwargs):
        self._doit()
        return self._result.isalpha(*args, **kwargs)

    def isdigit(self, *args, **kwargs):
        self._doit()
        return self._result.isdigit(*args, **kwargs)

    def islower(self, *args, **kwargs):
        self._doit()
        return self._result.islower(*args, **kwargs)

    def isspace(self, *args, **kwargs):
        self._doit()
        return self._result.isspace(*args, **kwargs)

    def istitle(self, *args, **kwargs):
        self._doit()
        return self._result.istitle(*args, **kwargs)

    def isupper(self, *args, **kwargs):
        self._doit()
        return self._result.isupper(*args, **kwargs)

    def join(self, *args, **kwargs):
        self._doit()
        return self._result.join(*args, **kwargs)

    def ljust(self, *args, **kwargs):
        self._doit()
        return self._result.ljust(*args, **kwargs)

    def lower(self, *args, **kwargs):
        self._doit()
        return self._result.lower(*args, **kwargs)

    def lstrip(self, *args, **kwargs):
        self._doit()
        return self._result.lstrip(*args, **kwargs)

    def partition(self, *args, **kwargs):
        self._doit()
        return self._result.partition(*args, **kwargs)

    def replace(self, *args, **kwargs):
        self._doit()
        return self._result.replace(*args, **kwargs)

    def rfind(self, *args, **kwargs):
        self._doit()
        return self._result.rfind(*args, **kwargs)

    def rindex(self, *args, **kwargs):
        self._doit()
        return self._result.rindex(*args, **kwargs)

    def rjust(self, *args, **kwargs):
        self._doit()
        return self._result.rjust(*args, **kwargs)

    def rpartition(self, *args, **kwargs):
        self._doit()
        return self._result.rpartition(*args, **kwargs)

    def rsplit(self, *args, **kwargs):
        self._doit()
        return self._result.rsplit(*args, **kwargs)

    def rstrip(self, *args, **kwargs):
        self._doit()
        return self._result.rstrip(*args, **kwargs)

    def split(self, *args, **kwargs):
        self._doit()
        return self._result.split(*args, **kwargs)

    def splitlines(self, *args, **kwargs):
        self._doit()
        return self._result.splitlines(*args, **kwargs)

    def startswith(self, *args, **kwargs):
        self._doit()
        return self._result.startswith(*args, **kwargs)

    def strip(self, *args, **kwargs):
        self._doit()
        return self._result.strip(*args, **kwargs)

    def swapcase(self, *args, **kwargs):
        self._doit()
        return self._result.swapcase(*args, **kwargs)

    def title(self, *args, **kwargs):
        self._doit()
        return self._result.title(*args, **kwargs)

    def translate(self, *args, **kwargs):
        self._doit()
        return self._result.translate(*args, **kwargs)

    def upper(self, *args, **kwargs):
        self._doit()
        return self._result.upper(*args, **kwargs)

    def zfill(self, *args, **kwargs):
        self._doit()
        return self._result.zfill(*args, **kwargs)



class PashCommand:
    def __init__(self, cmdname):
        self._cmdname = cmdname

    def __call__(self, *args, **kwargs):
        posargs = " ".join(args)
        flags = []
        for k,v in kwargs.iteritems():
            if len(k) == 1:
                dash = '-'
            else:
                dash = '--' 
            if type(v) == bool:
                arg = ""
            elif type(v) in (tuple, list):
                arg = " ".join(v)
            else:
                arg = str(v)
            flags.append('%s%s %s' % (dash, k, arg))
        cmd = "%s %s %s" % (self._cmdname, posargs, " ".join(flags))
        return oldmod.PashResult(cmd)


class pash:
    def __getattr__(self, name):
        if name[0] == '_': # drop initial _
            name = name[1:]
        name.replace('_', '-')
        return oldmod.PashCommand(name)

sys.modules[__name__] = pash()